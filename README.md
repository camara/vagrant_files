# README #

The steps are necessary to get a Python/Dajngo application up and running in this vagrant environment: 

### .bashrc file and over all configuration ###
First install the following: 
``` Bash
sudo apt-get update
sudo apt-get install python-pip libpq-dev python-dev postgresql
sudo pip install virtualenv virtualenvwrapper
```

Then copy and pase the following in your terminal 

``` Bash 
echo"
alias dj.server='python manage.py runserver 0.0.0.0:8888'

alias dj='python manage.py'

alias dj.shell='python manage.py shell_plus'

alias dj.mk='python manage.py makemigrations'

alias dj.migrate='python manage.py migrate'


export WORKON_HOME=~/Envs
source /usr/local/bin/virtualenvwrapper.sh

alias v='workon'
alias v.deactivate='deactivate'
alias v.mk='mkvirtualenv'
alias v.rm='rmvirtualenv'
alias v.switch='workon'
alias v.add2virtualenv='add2virtualenv'
alias v.cdsitepackages='cdsitepackages'
alias v.cd='cdvirtualenv'
alias v.lssitepackages='lssitepackages'

cd /vagrant " >> ~/.bashrc
```



### What is Virtualenv ###
A python environment manager that creates different Python environments for installing different versions of Python packages, to use it  you just need to run in your vagrant terminal: 

``` Bash
#to make a new environment
v.mk environment_name 
#to start environment (usually when login to vagrant ssh)
v environment_name
#to switch to a different environment
workon 
```

### Python/Django specific instructions ###

Every Django project you'll get, will have a requirement file containing all the python packages needed for the project, after making a new environment like described above you just need to run the following commands from the project directory: 
``` Bash 
pip install -r requirements.txt
``` 
After installing the all the needed Python package you'll need to create a database that matches the name of the database in the Django Settings file, located in a directory like project/project_name/settings.py

``` Bash
#this command will log you in, postgres terminal 
sudo su postgres
#Create a database named after Settings.py database
createdb db_name
#Create a superuser, same user and password in settings.py database credentials
createuser -P 
#follow instructions then exit
exit
```
Afterwards you'll need to use django to migrate the models written in Python, into the database, in the django directory, run the following command: 
``` Bash 
dj.migrate
```
Then create a superuser to access the admin page with, and be able to fill the database with dummy data, the admin page is located at localhost:8000/admin/ run the following command:
``` Bash
dj createsuperuser
```
Finally run the server using this command 
``` Bash 
dj.server
```
